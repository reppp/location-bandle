<?php

namespace LocationBundle\Services;

use LocationBundle\Entity\LocationEntity;
use LocationBundle\Exception\FailedException;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Component\Serializer\Serializer;

class LocationSerializer
{
    private $serializer;

    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    public function deserialize($jsonString): LocationEntity
    {
        $data = json_decode($jsonString, true);
        if ($this->validate($data)) {

            $entity = $this->serializer->denormalize($data['data'], LocationEntity::class, 'json');
            $entity->setSuccess($data['success']);

            return $entity;
        }
        throw new FailedException('Invalid JSON format');
    }

    public function serialize(array $data): string
    {
        if ($this->validate($data)) {
            return $this->serializer->serialize($data, 'json');
        }

        throw new FailedException('Invalid data');
    }

    private function validate($data): bool
    {
        return is_array($data) && isset($data['data'], $data['success']);
    }

}