<?php

namespace LocationBundle\Services;

use LocationBundle\Entity\LocationEntity;
use RequestBundle\Exception\ResponseException;
use RequestBundle\Services\ClientInterface;

class Api
{
    private $client;

    private $serializer;

    private $uri;

    public function __construct($client, $serializer)
    {
        $this->client = $client;
        $this->serializer = $serializer;
        $this->uri = '0.0.0.0';
    }

    public function setUri($uri)
    {
        $this->uri = $uri;
    }

    public function getLocation(): LocationEntity
    {
        $response = $this->client->get($this->uri);
        if ($response->getStatusCode() !== ClientInterface::HTTP_CODE_STATUS) {
            throw new ResponseException('The server response error '.$response->getStatusCode(), 0);
        }

        $entity = $this->serializer->deserialize($response->getBody());

        if (!$entity->getSuccess()) {
            $code = $entity->getCode() ?? -1;
            $message = $entity->getMessage() ?? 'Unknown error';

            throw new ResponseException($message, $code);
        }

        return $entity;
    }
}