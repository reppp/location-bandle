<?php

namespace LocationBundle\Entity;

class LocationEntity
{

    private $success = false;

    private $locations = [];

    private $code;

    private $message;

    public function getSuccess(): bool
    {
        return $this->success;
    }

    public function setSuccess($success)
    {
        $this->success = (bool)$success;

        return $this;
    }

    public function getLocations(): array
    {
        return $this->locations;
    }

    public function setLocations(array $locations)
    {
        $this->locations = $locations;

        return $this;
    }

    public function addLocation(array $location)
    {
        $this->locations[] = $location;
    }

    public function getCode(): int
    {
        return (int)$this->code;
    }

    public function setCode(?string $code)
    {
        $this->code = $code;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $errorMessage)
    {
        $this->message = $errorMessage;

        return $this;
    }
}