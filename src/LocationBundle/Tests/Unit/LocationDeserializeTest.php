<?php

namespace LocationBundle\Tests\Unit;

use LocationBundle\Exception\FailedException;
use LocationBundle\Tests\BaseTestClass;
use LocationBundle\Tests\Data\ApiData;

class LocationDeserializeTest extends BaseTestClass
{
    private $serializer;

    public function setUp()
    {
        parent::setUp();
        $this->serializer = $this->container->get('app.location.serializer');
    }

    public function testSuccessResponse()
    {
        $jsonString = ApiData::get('locationSuccess.json');
        $response = $this->serializer->deserialize($jsonString);
        $data = json_decode($jsonString, true);

        $this->assertEquals(null, $response->getMessage());
        $this->assertEquals(null, $response->getCode());
        $this->assertEquals(true, $response->getSuccess());
        $this->assertEquals($data['data']['locations'], $response->getLocations());
    }

    public function testErrorResponse()
    {
        $jsonString = ApiData::get('locationError.json');
        $response = $this->serializer->deserialize($jsonString);
        $data = json_decode($jsonString, true);

        $this->assertEquals($data['data']['message'], $response->getMessage());
        $this->assertEquals($data['data']['code'], $response->getCode());
        $this->assertEquals(false, $response->getSuccess());
        $this->assertEquals([], $response->getLocations());
    }

    public function testInvalidData()
    {
        $this->expectException(FailedException::class);
        $this->serializer->deserialize(ApiData::get('locationInvalidData.json'));
    }

    public function testInvalidSuccess()
    {
        $this->expectException(FailedException::class);
        $this->serializer->deserialize(ApiData::get('locationInvalidSuccess.json'));
    }

}