<?php

namespace LocationBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BaseTestClass extends WebTestCase
{
    public $client;

    public $container;

    public function setUp()
    {
        $this->initializeHelpers();
    }

    protected function initializeHelpers()
    {
        $this->client = static::createClient();
        $this->client->enableProfiler();
        $this->container = $this->client->getContainer();
    }
}