<?php
namespace LocationBundle\Tests\Data;

class ApiData
{
    public static function get($fileName)
    {
        return file_get_contents(__DIR__.DIRECTORY_SEPARATOR.$fileName);
    }
}