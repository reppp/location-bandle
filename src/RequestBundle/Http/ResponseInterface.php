<?php

namespace RequestBundle\Http;

use RequestBundle\Exception\ResponseException;

interface ResponseInterface
{

    public function getStatusCode(): int;

    public function getHeaders(): array;

    public function getBody(): string;
}