<?php


namespace RequestBundle\Http;

use RequestBundle\Exception\ResponseException;

class ResponseGuzzle implements ResponseInterface
{
    private $response;

    public function __construct($response)
    {
        $this->response = $response;
    }

    public function getStatusCode(): int
    {
        return $this->response->getStatusCode();
    }

    public function getHeaders(): array
    {
        return $this->response->getHeaders();
    }

    public function getBody(): string
    {
        try {
            return $this->response->getBody()->getContents();
        } catch (\RuntimeException $exception) {
            throw new ResponseException($exception->getMessage(), 0, $exception);
        }
    }
}