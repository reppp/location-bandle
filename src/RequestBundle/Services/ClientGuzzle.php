<?php

namespace RequestBundle\Services;

use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\RequestOptions as Options;
use GuzzleHttp\Client as GuzzleClient;
use RequestBundle\Exception\RequestException;
use RequestBundle\Http\ResponseGuzzle;
use RequestBundle\Http\ResponseInterface;

class ClientGuzzle implements ClientInterface
{
    const HTTP_TIMEOUT         = 7;
    const HTTP_CONNECT_TIMEOUT = 7;

    private $client;

    public function __construct()
    {
        $this->client = $this->createClientGuzzle();
    }

    public function request($method, $uri, array $options = []): ResponseInterface
    {
        try {
            return new ResponseGuzzle($this->client->request($method, $uri, $options));
        } catch (TransferException $exception) {
            throw new RequestException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    public function post($uri, array $options = []): ResponseInterface
    {
        return $this->request(self::METHOD_POST, $uri, $options);
    }

    public function get($uri, array $options = []): ResponseInterface
    {
        return $this->request(self::METHOD_GET, $uri, $options);
    }

    private function createClientGuzzle(): GuzzleClient
    {
        $options = [
            Options::VERIFY          => false,
            Options::HTTP_ERRORS     => false,
            Options::TIMEOUT         => self::HTTP_TIMEOUT,
            Options::CONNECT_TIMEOUT => self::HTTP_CONNECT_TIMEOUT,
            Options::DEBUG           => true,
            Options::ALLOW_REDIRECTS => false,
        ];

        return new GuzzleClient($options);
    }
}