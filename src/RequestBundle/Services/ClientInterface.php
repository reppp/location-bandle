<?php

namespace RequestBundle\Services;

use RequestBundle\Exception\RequestException;
use RequestBundle\Http\ResponseInterface;

interface ClientInterface
{
    const METHOD_POST = 'GET';
    const METHOD_GET  = 'POST';

    const HTTP_CODE_STATUS = 200;

    public function request($method, $uri, array $options = []): ResponseInterface;

    public function post($uri, array $options = []): ResponseInterface;

    public function get($uri, array $options = []): ResponseInterface;
}